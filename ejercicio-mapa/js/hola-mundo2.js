var map;
function Coordinates(lat, lng) {
    this.lat = lat;
    this.lng = lng;
}

function State(id, name) {
    this.id = id;
    this.name = name;
}

function Branch(name, address, stateId, lat, lng, imageUrl) {
    this.name = name;
    this.address = address;
    this.stateId = stateId;
    this.imageUrl = imageUrl;
    this.coordinates = new Coordinates(lat, lng);
}

/**
 * Contains all the available states
 * @type {Array}
 */
var states = [
    new State(1, 'Jalisco'),
    new State(2, 'Colima')
];

/**
 * Contains all the available branches
 * @type {Array}
 */
var branches = [
    new Branch('Andares', 'Paseo andares', 1, 20.709620, -103.412208),
    new Branch('Autonoma', 'Circuito universitario', 1, 20.695198, -103.416982),
    new Branch('Colima', '3er anillo periferico', 2, 19.266015, -103.697682)
];


window.addEventListener('load', inicializarEventos);

function inicializarEventos() {

    // init markers
    for (var i in branches) {
        var branch = branches[i];
        branch.marker = new google.maps.Marker({
            position: branch.coordinates,
            title: branch.name
        });

        branch.marker.addListener('click', function(){
            branch.infoWindow.open(map, this);
        });
    }

    // init info windows
    for ( i in branches) {
        var branch = branches[i];
        branch.infoWindow = new google.maps.InfoWindow({
            content: 'Hello humans, ima info window'
        });
    }

    updateBranchesDOM();
    updateBranchesMarkers();

    var select = document.getElementById('select_state');
    select.addEventListener('change', function () {
        updateBranchesDOM();
        updateBranchesMarkers();
    });
}

function updateBranchesDOM() {
    var branchesContainer = document.getElementById('states_info_container');
    branchesContainer.innerHTML = "";

    var statesSelect = document.getElementById('select_state');
    var selectedStateId = statesSelect.options[statesSelect.selectedIndex].value;

    for (var i in branches) {
        if (selectedStateId == 0) {
            var newBranchNodes = createBranchNodes(branches[i]);
            branchesContainer.appendChild(newBranchNodes);
        }
        else if (branches[i].stateId == selectedStateId) {
            var newBranchNodes = createBranchNodes(branches[i]);
            branchesContainer.appendChild(newBranchNodes);
        }
    }
}

function updateBranchesMarkers() {
    for (var i in branches) {
        branches[i].marker.setMap(null);
    }

    var statesSelect = document.getElementById('select_state');
    var selectedStateId = statesSelect.options[statesSelect.selectedIndex].value;

    for (var i in branches) {
        if (selectedStateId == 0) {
            branches[i].marker.setMap(map);
        }
        else if (branches[i].stateId == selectedStateId) {
            branches[i].marker.setMap(map);
        }
    }
}

function createBranchNodes(branch) {
    // main container
    var well = document.createElement('div');
    well.className = "well";

    // image
    var image = document.createElement('img');
    image.className = "img-responsive";

    // title
    var title = document.createElement('h3');
    title.appendChild(document.createTextNode(branch.name));

    // direction
    var direction = document.createElement('p');
    direction.appendChild(document.createTextNode(branch.address));

    // add elements to main  container
    well.appendChild(image);
    well.appendChild(title);
    well.appendChild(direction);

    return well;
}

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 19.266015, lng: -103.697682},
        zoom: 15
    });

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(userAcceptsLocation);
    }
}

function userAcceptsLocation(position){
    var initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    map.setCenter(initialLocation);
}