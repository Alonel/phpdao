<?php
include "dao.php";
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title> Create Sucursal</title>
    <link rel="stylesheet" type="text/css" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script
        src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </head>
  <body>
    <div class="container">
        <h1>Amin zone</h1>
        <h2>Create Sucursal</h2>
        <nav>
            <a href="admin.php" class="btn btn-primary"><i class="glyphicon glyphicon-chevron-left"></i> Volver</a>
        </nav>
         <form class="form-orizontal" action="save-branch.php" method="post">
           <label class="control-label col-sm-2">Sucursal</label>
           <div class="col-md-10">
                <input type="text" class="from-control" name="name">
           </div>

        </form>
     </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Estado: </label>
                <div class="col-sm-10">
                    <select class="select" name="state_id">
                         <?php
                           $estados = db_get_all('states');
                           while ($estado=mysqli_fetch_array($estados)) {
                          ?>
                             <option value="<?php $estado['id'] ?>">  <?= $estado['name'] ?>     </option>
                          <?php 
                            }
                         ?>
                         
                    </select>
                </div>
            </div>
         
       <div class="form-group">
             <div class="col-md-12 text-right" >
             <div>
                <button type="button" name="button" class="btn btn-info">Enviar  </button>        
             </div>
           </div>

        </div>



  </body>
</html>
