<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <h1>Admin zone</h1>
    <hr>
    <div class="row">
        <div class="col-sm-6">
            <div class="well">
                <h3>Estados</h3>
                <ul class="nav nav-pills nav-stacked">
                    <li role="presentation"><a href="states.php">Ver todos</a></li>
                    <li role="presentation"><a href="create-state.php">Crear</a></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="well">
                <h3>Sucursales</h3>
                <ul class="nav nav-pills nav-stacked">
                    <li role="presentation"><a href="branches.php">Ver todas</a></li>
                    <li role="presentation"><a href="create-branch.php">Crear</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
</body>
</html>
